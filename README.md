## Начало работы

1. Зарегистрируйте бесплатный аккаунт на https://aws.amazon.com/ru/
После регистрации зайдите в консоль управления AWS и зарегистрируйте домен (в данном примере testlab24.click)
2. Сгенерируйте SSH ключи для доступа к сервисам
3. Установите Terraform согласно документации
https://learn.hashicorp.com/tutorials/terraform/install-cli
4. Установите Ansible согласно документации
https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html

## Создание и конфигурация gitlab-runner

1. Склонируйте репозиторий https://gitlab.com/sn216362/skillbox-diploma.git

```
git clone https://gitlab.com/sn216362/skillbox-diploma.git
```

Создание gitlab-runner описано в папке skillbox-diploma/infra/gitlab-runner/terraform проекта в файле main.tf<br>
Создается инстанс ubuntu с минимальной конфигурацией t2.micro, а также балансировщик нагрузки с внешним адресом Elastic IP<br>
В разделе Route 53 Settings описывается создание записей для поддомена ci.testlab24.click, а также перенаправление нашего сервера на указанный домен<br>

2. Выполните последовательно команды:

```
cd skillbox-diploma/infra/gitlab-runner/terraform
terraform init # инициализирует Terraform и необходимые компоненты для запуска окружения
terraform apply # запускает процесс создания окружения
```

3. Окружение для gitlab-runner создано<br>
Далее необходимо установить сам gitlab-runner<br>
Делается это с помощью плейбука Ansible, описанном в файле skillbox-diploma/infra/gitlab-runner/ansible/gitlab-runner_install.yml проекта<br>
Плейбук реализован с помощью ролей<br>

4. Рассмотрим подробнее данный playbook<br>
Для доступа к серверам используется динамическая инвентаризация, реализованная с помощью плагина aws_ec2<br>
Укажем это в файле ansible.cfg:
```
inventory         = aws_ec2.yaml
```
Токен Gitlab указывается в переменной gitlab_token
```
skillbox-diploma/infra/gitlab-runner/ansible/roles/gitlab-runner/vars/main.yml
```

Согласно документации Ansible, все основные таски при использовании роли, описаны в папке skillbox-diploma/gitlab-runner/ansible/roles/docker/tasks<br>
В файле skillbox-diploma/infra/gitlab-runner/ansible/vars/default.yml указаны некоторые переменные для доступа к окружению для запуска нашего плейбука

5. Запуск плейбука:
```
cd skillbox-diploma/infra/gitlab-runner/ansible
ansible-playbook gitlab-runner_install.yml
```

Плейбук устанавливает Docker и запускает gitlab-runner в контейнере на сервере, указанном в разделе hosts

## Создание инфраструктуры с помощью Terraform

В репозиторий infra выложен для проверки ansible- и terraform-код для развёртывания окружения.

Проект состоит из четырех частей:
   - создание инфраструктуры с помощью Terraform (два тестовых сервера с приложением, два продакшн сервера с приложением,  балансировщики нагрузки, сервер мониторинга)
   - установка docker и node-exporter на созданные сервера
   - запуск Prometheus и Grafana в контейнерах Docker на сервере monitoring.testlab24.click
   - доставка приложения на созданные сервера

Для работы необходимо задать переменные в Gitlab Settings - CI/CD - Variables
```
AWS_ACCESS_KEY_ID
AWS_DEFAULT_REGION
AWS_INSTANCE_TYPE
AWS_SECRET_ACCESS_KEY
SSH_PRIVATE_KEY
SSH_PUBLIC_KEY
```
Для создания инфраструктуры необходимо запустить gitlab-runner, который запускает пайплайн проекта в Gitlab-CI

## Структура .gitlab-ci.yml

Пайплайн состоит из двух частей:
```
.gitlab-ci.yml - основной файл пайплайна, отвечающий за доставку приложений на наши сервера

/infra/terraform/terraform.gitlab-ci.yml - создание инфраструктуры в AWS (создание вируальных машин, DNS-записей)

/monitoring/monitoring.gitlab-ci.yml - запуск Prometheus и Grafana в контейнерах Docker на сервере monitoring.testlab24.click
```
Тесты проходят автоматически после удовлетворения зависимостей<br>
Запуск инфраструктуры и установка приложений настроены на ручной запуск

Пайплайн последовательно выполняет следующие действия:
```
   - build infra in aws - тестирование инфраструктурного кода в AWS
   - deploy infra in aws - создание инфраструктуры в AWS (ручной запуск)
   - docker install - установка docker на все сервера
   - service install - доставка приложения на сервера (ручной запуск)
   - monitoring install - установка системы мониторинга (ручной запуск)
   - destroy infra in aws - уничтожение инфраструктуры в AWS (ручной запуск)
```
Для работы пайплайна используются docker образы terraform и ansible<br>
Создание образа abacabba/ansible:v1 описано в папке docker проекта. Для удобства образ выгружен на hub.docker.com<br>
Образ registry.gitlab.com/gitlab-org/terraform-images/stable:latest является стандартным образом Gitlab<br>

## Описание плейбуков Ansible

В проекте используется динамическая инвентаризация AWS EC2<br>
Пример конфигурации:<br>
skillbox-diploma/monitoring/ansible.cfg
```
inventory         = aws_ec2.yaml
```

skillbox-diploma/infra/ansible/infra_install.yml<br>
Устанавливает Docker и Prometheus Node-exporter на все сервера<br>

skillbox-diploma/monitoring/monitoring_install.yml<br>
Запускает систему мониторинга Prometheus и Grafana в Docker контейнере на сервере мониторинга<br>

Файл конфигурации для системы мониторинга генерируется из шаблона, расположенного в папке skillbox-diploma/monitoring/roles/prometheus/templates

В случае успешного запуска<br>
Prometheus доступен по ссылке http://monitoring.testlab24.click:9090<br>
Grafana доступна по ссылке http://grafana.testlab24.click:3000<br>

В репозиторий service выложен для проверки ansible-код для развертывания сервиса<br>
 
skillbox-diploma/service/app_install.yml<br>
Доставка приложения и его запуск в Docker контейнере на тестовых и продакшн серверах<br>
Окружение выбирается исходя из пайплайна (Dev и Prod stages)<br>
```
- hosts: 
    - _{{ ENV }}_Server_in_Auto_Scalling_Group
```
Приложение и файлы конфигурации копируются из стороннего репозитория на наши сервера, указанные в группе Auto Scalling Group<br>
https://github.com/bhavenger/skillbox-diploma.git

## Сбор логов

Сбор логов осуществляется на сервере efk.testlab24.click<br>
По желанию данный инстанс можно развернуть при помощи Terraform на площадках AWS, Google или Yandex<br>
Расчеты были сделаны из расчета 2 vCPU, 2Gb RAM, 30Gb HDD<br>

В нашем случае инстанс собран на виртуальной машине в собственном Дата-центре<br>
При момощи плейбука logs\ansible\efk_install.yml на машину устанавливается стек EFK (Elasticsearch + Fluentd + Kibana) в докер-контейнерах<br>

Агенты Fluent для отправки логов устанавливаются и конфигурируются при помощи роли Ansible infra\ansible\roles\td-agent<br>

После запуска пайплайна доступен сервер http://efk.testlab24.click:5601/<br>


## Итоги работы

1. Приложение доставлено на наши сервера<br>
Проверить это можно по ссылке:<br>
http://dev.testlab24.click<br>
http://prod.testlab24.click<br>

2. Развернута система мониторинга на базе Prometheus, расположена по ссылке:<br>
http://monitoring.testlab24.click:9090<br>
В данной системе можно собирать метрики со всех серверов нашей инфраструктуры.

3. Развернута Grafana, расположена по ссылке:<br>
http://grafana.testlab24.click:3000<br>
В данной системе можно визуально посмотреть нагрузку всех серверов нашей инфраструктуры с помощью удобных дашбордов.

4. После проверки задания не забудьте удалить созданные при помощи Terraform ресурсы<br>
Для этого запустите destroy в нашем пайплайне

Примечание<br>
В файле .gitignore описаны файлы, которые не рекомендуется выкладывать в общий доступ<br>
Например, terraform.tfstate
<br>