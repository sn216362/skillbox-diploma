resource "aws_eip" "monitoring_static_ip" {
  instance = aws_instance.monitoring_server.id
  tags = {
    Name  = "Monitoring Server IP"
  }
}

# Ищем образ с последней версией Ubuntu
data "aws_ami" "ubuntu_monitoring" {
  owners      = ["099720109477"]
  most_recent = true
  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-focal-20.04-amd64-server-*"]
  }
}

# Запускаем инстанс
resource "aws_instance" "monitoring_server" {
  # с выбранным образом
  ami                    = data.aws_ami.ubuntu_monitoring.id
  # и размером (количество ЦПУ и памяти зависит от этой директивы)
  instance_type          = var.instance_type
  vpc_security_group_ids = [aws_security_group.monitoring_server.id]
  #user_data = file("user_data.sh")
  key_name = "id_rsa"
  tags = {
    AMI =  "${data.aws_ami.ubuntu_monitoring.id}"
    Name  = "Monitoring Server IP"
    Env = "Production"
    Tier = "Frontend"
  }

  lifecycle {
    create_before_destroy = true
  }

}

resource "aws_security_group" "monitoring_server" {
  name        = "Monitoring Server Security Group"
  description = "Security group for accessing traffic to our Monitoring Server"


  dynamic "ingress" {
    for_each = ["22","3000","9090","9093","9100","9115"]
    content {
      from_port   = ingress.value
      to_port     = ingress.value
      protocol    = "tcp"
      cidr_blocks = ["0.0.0.0/0"]
    }
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name  = "Monitoring Server Security Group"
  }
}
