# Выведем IP адрес сервера
output "monitoring_site_ip" {
  description = "Elatic IP address assigned to our Monitoring"
  value       = aws_eip.monitoring_static_ip.public_ip
}
