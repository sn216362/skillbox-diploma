data "aws_route53_zone" "testlab24" {
  name = "testlab24.click."
}

resource "aws_route53_record" "monitoring" {
   zone_id = data.aws_route53_zone.testlab24.zone_id
   name    = "monitoring.${data.aws_route53_zone.testlab24.name}"
  type    = "A"
  ttl     = "300"
  records = ["${aws_eip.monitoring_static_ip.public_ip}"]
}

resource "aws_route53_record" "grafana" {
   zone_id = data.aws_route53_zone.testlab24.zone_id
   name    = "grafana.${data.aws_route53_zone.testlab24.name}"
  type    = "CNAME"
  ttl     = "300"
  records = ["monitoring.${data.aws_route53_zone.testlab24.name}"]
}
