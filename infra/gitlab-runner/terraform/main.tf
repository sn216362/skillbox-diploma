# Указываем, что мы хотим разворачивать окружение в AWS
provider "aws" {
  region = "eu-central-1"
}

resource "aws_eip" "my_static_ip" {
  instance = aws_instance.ci_server.id
  tags = {
    Name  = "CI Server IP"
  }
}

# Ищем образ с последней версией Ubuntu
data "aws_ami" "ubuntu" {
  owners      = ["099720109477"]
  most_recent = true
  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-focal-20.04-amd64-server-*"]
  }
}

# Запускаем инстанс
resource "aws_instance" "ci_server" {
  # с выбранным образом
  ami                    = data.aws_ami.ubuntu.id
  # и размером (количество ЦПУ и памяти зависит от этой директивы)
  instance_type          = "t2.micro"
  vpc_security_group_ids = [aws_security_group.ci_server.id]
  #user_data = file("user_data.sh")
  key_name = "id_rsa"
  tags = {
    AMI =  "${data.aws_ami.ubuntu.id}"
    Name  = "CI Server IP"
    Env = "Production"
    Tier = "Frontend"
  }

  lifecycle {
    create_before_destroy = true
  }

}

resource "aws_security_group" "ci_server" {
  name        = "CI Server Security Group"
  description = "Security group for accessing traffic to our CI Server"


  dynamic "ingress" {
    for_each = ["22","8093"]
    content {
      from_port   = ingress.value
      to_port     = ingress.value
      protocol    = "tcp"
      cidr_blocks = ["0.0.0.0/0"]
    }
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name  = "CI Server SecurityGroup"
  }
}

# Выведем IP адрес сервера
output "my_web_site_ip" {
  description = "Elatic IP address assigned to our CI"
  value       = aws_eip.my_static_ip.public_ip
}
