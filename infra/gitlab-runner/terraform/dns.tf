# Route 53 Settings
data "aws_route53_zone" "testlab24" {
  name = "testlab24.click."
}

resource "aws_route53_record" "ci" {
   zone_id = data.aws_route53_zone.testlab24.zone_id
   name    = "ci.${data.aws_route53_zone.testlab24.name}"
  type    = "A"
  ttl     = "300"
  records = ["${aws_eip.my_static_ip.public_ip}"]
}

