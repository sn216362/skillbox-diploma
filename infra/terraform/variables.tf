variable "access_key" {
    type = string
    default = "$AWS_ACCESS_KEY_ID"
}

variable "secret_key" {
    type = string
    default = "$AWS_SECRET_ACCESS_KEY"
}

variable "region" {
    type = string
    default = "$AWS_DEFAULT_REGION"
}

variable "instance_type" {
    type = string
    default = "$AWS_INSTANCE_TYPE"
}

variable "aws_security_group_name" {
    type = string
    default = "$ENV-Security-Group"
}

variable "instance_in_aws_autoscaling_group_name" {
    type = string
    default = "$ENV-Server-in-Auto-Scalling-Group"
}

variable "elastic_load_balancer_name" {
    type = string
    default = "$ENV-highly-available-elb"
}

variable "srv_static_ip" {
    type = string
    default = "$ENV-App-Server-IP-For-Ansible"
}

variable "instance_name" {
    type = string
    default = "$ENV-App-Server"
}

variable "aws_route53_record_name" {
    type = string
    default = "$ENV.testlab24.click"
}
