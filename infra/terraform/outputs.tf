# Выведем в консоль DNS имя нашего сервера 
output "web_loadbalancer_url" {
  value = aws_elb.srv.dns_name
}

# Выведем IP адрес сервера
output "srv_web_site_ip" {
  description = "Elatic IP address assigned to our ReactJS Server"
  value       = aws_eip.srv_static_ip.public_ip
}


