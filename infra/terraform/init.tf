# Указываем, что мы хотим разворачивать окружение в AWS
provider "aws" {
  access_key = var.access_key
  secret_key = var.secret_key
  region = var.region
}
