# Route 53 Settings
data "aws_route53_zone" "testlab24" {
  name = "testlab24.click."
}

resource "aws_route53_record" "test" {
  zone_id = data.aws_route53_zone.testlab24.zone_id
  #name    = "$ENV.${data.aws_route53_zone.testlab24.name}"
  name = var.aws_route53_record_name
  type    = "CNAME"
  ttl     = "300"
  records = ["${aws_elb.srv.dns_name}."]
}

#resource "aws_route53_record" "www" {
#  zone_id = data.aws_route53_zone.testlab24.zone_id
#  name    = "testlab24.click"
#  type    = "A"
#
#  alias {
#    name                   = aws_elb.srv.dns_name
#    zone_id                = aws_elb.srv.zone_id
#    evaluate_target_health = true
#  }
#}
