data "aws_route53_zone" "testlab24" {
  name = "testlab24.click."
}

resource "aws_route53_record" "efk" {
   zone_id = data.aws_route53_zone.testlab24.zone_id
   name    = "efk.${data.aws_route53_zone.testlab24.name}"
  type    = "A"
  ttl     = "300"
  records = "65.108.76.143"
}